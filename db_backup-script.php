<?php

/* 
 * Copyright (C) 2017 Markus Mörbe <markus@moerbe.eu>
 * MOERBE Business Solutions
 *
 * CC BY-NC-SA 3.0 Deutschland
 *
 * Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
 * Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen
 * Bedingungen 3.0 Deutschland zugänglich. Um eine Kopie dieser
 * Lizenz einzusehen, konsultieren Sie
 * http://creativecommons.org/licenses/by-nc-sa/3.0/de/
 * oder wenden Sie sich brieflich an
 * Creative Commons, Postfach 1866, Mountain View, California, 94042, USA.
 */

// db_backup-script_iriemo_04.php

declare(strict_types=1);

@set_time_limit(0);

// 0: Normale Datei
// 1: GZip-Datei
$compression = 0;

//Falls Gzip nicht vorhanden, kein Gzip
if(!extension_loaded("zlib")) $compression = 0;

// Pfad zur aktuellen Datei
$path_parts = pathinfo(__FILE__);
$path = $path_parts['dirname'];
// Pfad zum Backup
$path .= '/backup_files/';

// Backup Optionen
// 0: Struktur und Daten in separaten Files ("__structure"; "__data")
// 1: Struktur und Daten in einem File ("__all")
$backup_option = 1;

//Dateieigenschaften
$cur_time = date( "Y-m-d--h-i-s");
$cur_date = date("Y-m-d");

// Konfigurations-Parameter
$params = [
    'db_host'=> '127.0.0.1',  //mysql host
    'db_uname' => 'root',  //user
    'db_password' => '', //pass

    'db_port' => '3306',
    'db_charset' => 'utf8',
    
    'db_exclude_tables' => [
                //'be_users',
    ], //tables to exclude
    'db_exclude_tables_data' => [
		'be_sessions',
                'cache_md5params',
                'cache_treelist',
                'cf_cache_hash',
                'cf_cache_hash_tags',
                'cf_cache_imagesizes',
                'cf_cache_imagesizes_tags',
                'cf_cache_pages',
                'cf_cache_pagesection',
                'cf_cache_pagesection_tags',
                'cf_cache_pages_tags',
                'cf_cache_rootline',
                'cf_cache_rootline_tags',
                'cf_extbase_datamapfactory_datamap',
                'cf_extbase_datamapfactory_datamap_tags',
                'cf_extbase_object',
                'cf_extbase_object_tags',
                'cf_extbase_reflection',
                'cf_extbase_reflection_tags',
                'cf_extbase_typo3dbbackend_queries',
                'cf_extbase_typo3dbbackend_queries_tags',
                'fe_sessions',
		'fe_session_data',
                'sys_log',
		'tx_extensionmanager_domain_model_extension',
    ] // table-data/content to exclude
];

$db_to_backup = [
    'typo3_us_wbs',
];


////////////////////////////////////////////////////////////////////////////////
$messages = [
    'success' => "<h3>Backup done!</h3>",
    'error' => "<h3>! ! ! ERROR ! ! !</h3>",
];

// Funktion für Verbindung zu einer bestimmten Datenbank
function connect_to_db(string $database) : mysqli {
    global $params;
    $mysqli = new mysqli($params['db_host'], $params['db_uname'], $params['db_password'], $database);
    if ($mysqli->connect_error) {
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    return $mysqli;
}

function do_backup_mysql_database(mysqli $mysqli, string $database) {
    global $path, $backup_option, $messages;

    $message_content = "";
    $content = "-- DATABASE: `" . $database . "` --\n";
    
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    
    $file_suffix = get_file_suffix($backup_option);
    
    if($backup_option == 0) {
	$content_s = $content . "-- STRUCTURE --\n\n";
	foreach (get_mysql_database_structure($mysqli) as $structure) {
	    $content_s .= $structure;
	}
	
	$message_content .= write_backup_file($database, $content_s, $file_suffix[0]) . "\n";
	
	$content_d = $content . "-- DATA --\n\n";
	foreach (get_mysql_database_data($mysqli) as $data) {
	    $content_d .= $data;
	}
	
	$message_content .= write_backup_file($database, $content_d, $file_suffix[1]) . "\n";
    } else {
	$content .= "-- STRUCTURE --\n\n";
	foreach (get_mysql_database_structure($mysqli) as $structure) {
	    $content .= $structure;
	}

	$content .= "-- DATA --\n\n";
	foreach (get_mysql_database_data($mysqli) as $data) {
	    $content .= $data;
	}
	$message_content .= write_backup_file($database, $content, $file_suffix[0]) . "\n";
    }
    message($messages['success'], $message_content);
    
} // do_backup_mysql_database

function get_mysql_database_structure(mysqli $mysqli) : array {
    global $params;
    
    $include_tables = [];

    $contents = [];
    $results = $mysqli->query("SHOW TABLES");
    
    while($row = $results->fetch_row()){       
        if (!in_array($row[0], $params['db_exclude_tables'])){
            $include_tables[] = $row[0];
        }
    }

    foreach($include_tables as $table){
	$content = '';
        $content .= "-- Table `" . $table . "` --\n";
       
        $results = $mysqli->query("SHOW CREATE TABLE " . $table);
        while($row = $results->fetch_array()){
            $content .= $row[1].";\n\n";
        }
	$contents[] = $content;
    }
    return $contents;

} // get_mysql_database_structure

function get_mysql_database_data(mysqli $mysqli) : array {
    global $params;
    
    $include_tables = [];
    $contents = [];
    $results = $mysqli->query("SHOW TABLES");
    
    while($row = $results->fetch_row()){       
        if (!in_array($row[0], $params['db_exclude_tables']) && !in_array($row[0], $params['db_exclude_tables_data'])){
            $include_tables[] = $row[0];
        }
    }

    foreach($include_tables as $table){
	$content = '';
        $content .= "-- Table `" . $table . "` --\n";
	
        $results = $mysqli->query("SELECT * FROM " . $table);
        $row_count = $results->num_rows;
        $fields = $results->fetch_fields();
        $fields_count = count($fields);
      
        if (in_array($table, $include_tables)){
            $insert_head = "INSERT INTO `" . $table."` (";
            for($i=0; $i < $fields_count; $i++){
                $insert_head  .= "`" . $fields[$i]->name . "`";
                    if($i < $fields_count-1){
                        $insert_head .= ', ';
                    }
            }
            $insert_head .=  ")";
            $insert_head .= " VALUES\n";
	    
        } else {
            $row_count = 0;
        }
               
        if($row_count > 0){
            $r = 0;
            while($row = $results->fetch_array()) {
                if(($r % 400)  == 0) {
                    $content .= $insert_head;
                }
                $content .= "(";
		for($j=0; $j<$fields_count; $j++) { 
		     
		    if (isset($row[$j])) {
			$row[$j] = str_replace("\n", "\\n", $mysqli->real_escape_string($row[$j]));
			
			switch($fields[$j]->type) {
			    
			    //    1=>'tinyint',	    x
			    //    2=>'smallint',    x
			    //    3=>'int',	    x
			    //    4=>'float',
			    //    5=>'double',
			    //    7=>'timestamp',   x
			    //    8=>'bigint',	    x
			    //    9=>'mediumint',   x
			    //    10=>'date',
			    //    11=>'time',
			    //    12=>'datetime',
			    //    13=>'year',
			    //    16=>'bit',
			    //    253=>'varchar',   s
			    //    254=>'char',	    s
			    //    246=>'decimal'
			    
			    // TODO
			    case 1: case 8: case 3:
				$content .= $row[$j];
				break;
			    default:
				$content .= "'" . $row[$j] . "'";
			}
		    }
		    else 
		    {   
			$content .= "''";
		    }     
		    if ($j<($fields_count-1))
		    {
			    $content.= ", ";
		    }      
		}
		
                if(($r+1) == $row_count || ($r % 400) == 399){
                    $content .= ");\n\n";
                } else {
                    $content .= "),\n";
                }
                $r++;
            }	
        } else {
	    $content .= "       --> empty or data excluded from export\n\n";
	}
	$contents[] = $content;
    }
    return $contents;

} // get_mysql_database_data

//Funktion um Backup auf dem Server zu speichern
function write_backup_file(string $database, string $content, string $file_suffix) : string {
    global $compression, $path, $cur_time;
    
    $file_syntax ="sql-backup__" . $database . "__" . $cur_time;
    
    $message = "Database: " . $database . "\n\n";
    
    if ($compression == 1) {
        $filetype = ".sql.gz";
        $compression_mode = "w9";

	$fp = gzopen($path . $file_syntax . $file_suffix . $filetype, $compression_mode);
	gzwrite ($fp, $content);
	gzclose ($fp);
	$message .= "File: " . $file_syntax . $file_suffix . $filetype . "\n";
    } else {
        $filetype = ".sql";
        $compression_mode = "w";
        
	$fp = fopen($path . $file_syntax . $file_suffix . $filetype, $compression_mode);
	fwrite ($fp, $content);
	fclose ($fp);
	$message .= "File: " . $file_syntax . $file_suffix . $filetype . "\n";
    }
    return $message;
} // write_backup_file

// Funktion, um Backup-File im gewünschten Pfad mit entsprechender Syntax zu erstellen
function get_file_suffix(int $backup_option) : array {
   global $messages;
    $file_suffix = [];

    switch ($backup_option) {
        case 0:
            $file_suffix[] =  "__structure";
            $file_suffix[] = "__data";
            break;
        case 1:
            $file_suffix[] = "__all";
            break;
        default:
	    $file_suffix[] = "__all";
            message($messages['error'], "Wrong configuration in: backup_option. \n Full backup done!");
            break;
    }
    return $file_suffix;
} // get_file_syntax

// Funktion zur Ausgabe einer Meldung
function message(string $type = '', string $message_content = '') {
    echo $type;
    echo "<p>$message_content</p>";

} // message()


// Methode aufrufen zum backup
foreach ($db_to_backup as $database) {
    $mysqli = connect_to_db($database);
    do_backup_mysql_database($mysqli, $database);
}




